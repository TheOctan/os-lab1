﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace Lab1
{
	public partial class Form1 : Form
	{
		private List<ProcessInfo> processes = new List<ProcessInfo>();
		private List<ProcessThread> threads = new List<ProcessThread>();

		public Form1()
		{
			InitializeComponent();
			initProcesses();
			initView();
		}

		private void initProcesses()
		{
            processes.Clear();
            threads.Clear();

			foreach (var process in Process.GetProcesses())
			{
				processes.Add(new ProcessInfo()
				{
					Id			= process.Id,
					User		= process.StartInfo.UserName,
					Memory		= process.PagedMemorySize64,
					Priority	= process.BasePriority,
					Thereads	= process.Threads.Count,
					Name		= process.ProcessName,
					TreadsList	= process.Threads
				});
			}
			foreach (ProcessThread thread in processes.First().TreadsList)
			{
				threads.Add(thread);
			}
		}

		private void initView()
		{
			dataGridView1.DataSource = processes.Select(process => new
			{
				process.Id,
				process.User,
				process.Name,
				process.Memory,
				process.Priority,
				process.Thereads
			}).ToList();

			dataGridView2.DataSource = threads.Select(thread => new
			{
				thread.Id,
				thread.CurrentPriority
			}).ToList();

            label1.Text = "Count processes" + processes.Count;
		}

		private void dataGridView1_SelectionChanged(object sender, EventArgs e)
		{
			var index = dataGridView1.CurrentRow.Index;
			threads.Clear();
			foreach (ProcessThread thread in processes[index].TreadsList)
			{
				threads.Add(thread);
			}
			dataGridView2.DataSource = threads.Select(thread => new
			{
				thread.Id,
				thread.CurrentPriority
			}).ToList();
		}

        private void button1_Click(object sender, EventArgs e)
        {
            initProcesses();
            initView();
        }
    }
}
