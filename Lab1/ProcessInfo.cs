﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
	class ProcessInfo
	{
		public int Id		{ get; set; }
		public string User	{ get; set; }
		public long Memory	{ get; set; }
		public int Priority { get; set; }
		public int Thereads { get; set; }
		public string Name	{ get; set; }
		public ProcessThreadCollection TreadsList { get; set; }
	}
}
